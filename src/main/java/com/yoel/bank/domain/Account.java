package com.yoel.bank.domain;

import java.time.LocalDate;

public class Account {
    private Amount balance;

    private final Operations operations;

    public Account(Amount balance, Operations operations) {
        this.balance = balance;
        this.operations = operations;
    }

    public void deposit(Amount amount) {
        balance = balance.plus(amount);
        Operation operation = new Operation("DEPOSIT ", LocalDate.now(), amount, balance);
        operations.add(operation);
    }

    public void withdrawal(Amount amount) {
            balance = balance.minus(amount);
        if (balance.isGreaterThan(Amount.of(0))) {
            Operation operation = new Operation("WITHDRAWAL ", LocalDate.now(), amount, balance);
            operations.add(operation);
        } else {
            balance = balance.plus(amount);
        }
    }

    public Amount getBalance() {
        return balance;
    }
}
