package com.yoel.bank.domain;

import java.text.DecimalFormat;

public class Amount {
    private final double value;

    public Amount(double value) {
        this.value = value;
    }

    public static Amount of(double value) {
        return new Amount(value);
    }

    public Amount plus(Amount amount) {
        return of(this.value + amount.value);
    }

    public Amount minus(Amount amount) {
        return of(this.value - amount.value);
    }

    public Boolean isGreaterThan(Amount amount){
        return this.value >= amount.value;
    }

    public String toDecimal() {
        return new DecimalFormat("00.00").format(value);
    }
}
