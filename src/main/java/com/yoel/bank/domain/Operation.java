package com.yoel.bank.domain;

import java.time.LocalDate;

public class Operation {
    private final String operation;
    private final LocalDate date;
    private final Amount amount;
    private final Amount balance;

    public Operation(String operation, LocalDate date, Amount amount, Amount balance) {
        this.operation = operation;
        this.date = date;
        this.amount = amount;
        this.balance = balance;
    }

    public String print() {
        return operation + " " + date + " " + amount.toDecimal() + " " + balance.toDecimal();
    }
}
