package com.yoel.bank.domain;

import java.util.LinkedList;
import java.util.List;

public class Operations {
    private final List<Operation> operations = new LinkedList<Operation>();

    public void add(Operation operation){
        operations.add(operation);
    }

    public StringBuilder print(){
        StringBuilder print = new StringBuilder();
        for (Operation operation : operations) {
            print.append(operation.print());
            print.append("<br>");
        }
        return print;
    }
}
