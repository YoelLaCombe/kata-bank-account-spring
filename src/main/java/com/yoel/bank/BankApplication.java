package com.yoel.bank;

import com.yoel.bank.domain.Account;
import com.yoel.bank.domain.Amount;
import com.yoel.bank.domain.Operations;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class BankApplication {
	public static Operations operations = new Operations();
	public Account account = new Account(Amount.of(0), operations);

	@GetMapping("/")
	String home() {
		return "Welcome to the Marciano Bank Corporation";
	}

	@GetMapping("/deposit/{amount}")
	String deposit(@PathVariable Double amount) {
		account.deposit(Amount.of(amount));
		return "Welcome to the Marciano Bank Corporation. You have " + account.getBalance().toDecimal() + " euros in your account now";
	}

	@GetMapping("/withdraw/{amount}")
	String withdraw(@PathVariable Double amount) {
		account.withdrawal(Amount.of(amount));
		return "Welcome to the Marciano Bank Corporation. You have " + account.getBalance().toDecimal() + " euros in your account now";
	}

	@GetMapping("/history")
	StringBuilder history() {
		return operations.print();
	}

	public static void main(String[] args) {
		SpringApplication.run(BankApplication.class, args);
	}
}