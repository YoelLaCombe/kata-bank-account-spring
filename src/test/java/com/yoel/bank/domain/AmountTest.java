package com.yoel.bank.domain;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class AmountTest {
    @Test
    void should_sum_amounts() {
        Amount twentySix = new Amount(26);

        Assertions.assertThat(twentySix.plus(twentySix)).isEqualToComparingFieldByField(Amount.of(52));
    }

    @Test
    void should_subtract_amounts() {
        Amount twentySix = new Amount(26);
        Amount fiftyTwo = new Amount(52);

        Assertions.assertThat(fiftyTwo.minus(twentySix)).isEqualToComparingFieldByField(twentySix);
    }

    @Test
    void should_transform_to_decimal_with_two_when_zero() {
        Amount twentySix = new Amount(0);

        Assertions.assertThat(twentySix.toDecimal()).isEqualTo("00,00");
    }

    @Test
    void should_transform_to_decimal_with_two_when_one() {
        Amount twentySix = new Amount(1);

        Assertions.assertThat(twentySix.toDecimal()).isEqualTo("01,00");
    }

    @Test
    void should_transform_to_decimal_with_two_when_integer() {
        Amount twentySix = new Amount(26);

        Assertions.assertThat(twentySix.toDecimal()).isEqualTo("26,00");
    }
}