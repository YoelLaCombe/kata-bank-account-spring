package com.yoel.bank;

import com.yoel.bank.domain.Account;
import com.yoel.bank.domain.Amount;
import com.yoel.bank.domain.Operations;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountTest {

    @Test
    public void should_add_deposit_in_account() {
        Account account = new Account(Amount.of(0), new Operations());
        account.deposit(Amount.of(26d));
        assertThat(account.getBalance()).isEqualToComparingFieldByField(Amount.of(26d));
    }

    @Test
    public void should_add_several_deposits_in_account() {
        Account account = new Account(Amount.of(0), new Operations());
        account.deposit(Amount.of(26d));
        account.deposit(Amount.of(26d));
        account.deposit(Amount.of(52d));
        assertThat(account.getBalance()).isEqualToComparingFieldByField(Amount.of(104d));
    }

    @Test
    public void should_add_withdrawal_in_account() {
        Account account = new Account(Amount.of(52d), new Operations());
        account.withdrawal(Amount.of(26d));
        assertThat(account.getBalance()).isEqualToComparingFieldByField(Amount.of(26d));
    }

    @Test
    public void should_add_several_withdrawal_in_account() {
        Account account = new Account(Amount.of(104d), new Operations());
        account.withdrawal(Amount.of(52d));
        account.withdrawal(Amount.of(26d));
        assertThat(account.getBalance()).isEqualToComparingFieldByField(Amount.of(26d));
    }

    @Test
    public void should_cancel_withdrawal_when_there_is_no_money_in_account() {
        Account account = new Account(Amount.of(0d), new Operations());
        account.withdrawal(Amount.of(26d));
        assertThat(account.getBalance()).isEqualToComparingFieldByField(Amount.of(0d));
    }

    @Test
    public void should_cancel_withdrawal_when_amount_to_withdraw_is_greater_than_balance() {
        Account account = new Account(Amount.of(26d), new Operations());
        account.withdrawal(Amount.of(52d));
        assertThat(account.getBalance()).isEqualToComparingFieldByField(Amount.of(26d));
    }

}