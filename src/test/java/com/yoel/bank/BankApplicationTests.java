package com.yoel.bank;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class BankApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void homeResponse() {
		String body = this.restTemplate.getForObject("/", String.class);
		assertThat(body).isEqualTo("Welcome to the Marciano Bank Corporation");
	}

	@Test
	public void depositResponse() {
		String body = this.restTemplate.getForObject("/deposit/26", String.class);
		assertThat(body).isEqualTo("Welcome to the Marciano Bank Corporation. You have 26,00 euros in your account now");
	}

	@Test
	public void withdrawResponse() {
		String body = this.restTemplate.getForObject("/withdraw/26", String.class);
		assertThat(body).isEqualTo("Welcome to the Marciano Bank Corporation. You have 00,00 euros in your account now");
	}

	@Test
	public void historyResponse() {
		this.restTemplate.getForObject("/deposit/52", String.class);
		this.restTemplate.getForObject("/withdraw/26", String.class);
		String body = this.restTemplate.getForObject("/history", String.class);
		assertThat(body).isEqualTo("DEPOSIT  "+ LocalDate.now() +" 52,00 52,00<br>WITHDRAWAL  "+ LocalDate.now() +" 26,00 26,00<br>");
	}
}
